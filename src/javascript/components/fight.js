import { controls } from '../../constants/controls';
import specKeyPress from '../helpers/keyPress';

const checkIsInBlockFn = (fighter) => {
  if (fighter.isInBlock) {
    return true;
  }
  return false;
};

const healthBarRender = (defender) => {
  const healthBarPerPercent = defender.healthInit / 100;
  const defenderHealthBar = Math.floor(defender.health / healthBarPerPercent);
  const defenderDom = document.getElementById(`${defender.playerPosition}-fighter-indicator`);
  defenderDom.style.width = defenderHealthBar < 0 ? 0 : `${defenderHealthBar}%`;
};

const attackFn = (attacker, defender) => {
  if (checkIsInBlockFn(attacker, true) || checkIsInBlockFn(defender)) return;

  const damage = getDamage(attacker, defender);

  defender.health -= damage;

  healthBarRender(defender);

  //console.log(`Player ${attacker.name} attacked. Player defender health - ${defender.health}`);
};

const criticalHitFn = (attacker, defender) => {
  if (checkIsInBlockFn(attacker)) return;

  if (attacker.lastCriticalHit) {
    if ((Date.now() - attacker.lastCriticalHit) / 1000 < 10) {
      console.log('not allowed. pls wait another 10s');
      return false;
    }
  }

  attacker.lastCriticalHit = Date.now();

  const damage = attacker.attack * 2;

  defender.health -= damage;

  healthBarRender(defender);
  //console.log(`criticalHitFn actions implementation... Enemy health ${defender.health}`);
};

const blockFn = (fighter) => {
  fighter.isInBlock = true;
};

const unBlockFn = (fighter) => {
  fighter.isInBlock = false;
};

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const winnerCheck = (firstFighter, secondFighter, resolveFn) => {
      if (firstFighter.health <= 0) resolveFn(secondFighter);
      if (secondFighter.health <= 0) resolveFn(firstFighter);
    };

    document.addEventListener('keypress', (e) => winnerCheck(firstFighter, secondFighter, resolve));

    specKeyPress(
      criticalHitFn.bind(null, firstFighter, secondFighter),
      () => {},
      ...controls.PlayerOneCriticalHitCombination
    );
    specKeyPress(
      criticalHitFn.bind(null, secondFighter, firstFighter),
      () => {},
      ...controls.PlayerTwoCriticalHitCombination
    );

    specKeyPress(blockFn.bind(null, firstFighter), unBlockFn.bind(null, firstFighter), controls.PlayerOneBlock);
    specKeyPress(blockFn.bind(null, secondFighter), unBlockFn.bind(null, secondFighter), controls.PlayerTwoBlock);

    specKeyPress(attackFn.bind(null, firstFighter, secondFighter), () => {}, controls.PlayerOneAttack);
    specKeyPress(attackFn.bind(null, secondFighter, firstFighter), () => {}, controls.PlayerTwoAttack);
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage < 0) return 0;
  return damage;
}

export function getHitPower(fighter) {
  return fighter.attack * Math.random() * 2 + 1;
}

export function getBlockPower(fighter) {
  return fighter.attack * Math.random() * 2 + 1;
}
