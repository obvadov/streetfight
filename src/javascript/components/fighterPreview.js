import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    fighterElement.appendChild(fighterImage);

    const defenseNode = createFighterTextElement(fighter, 'attack');

    fighterElement.appendChild(defenseNode);
  }

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterTextElement(fighter, field) {
  const rootElement = createElement({
    tagName: 'div',
    className: 'preview___div',
  });

  for (const key in fighter) {
    if (['_id', 'source'].includes(key)) continue;

    const element = createElement({
      tagName: 'div',
      className: 'preview___div',
    });

    element.innerHTML = `${key}: ${fighter[key]}`;

    rootElement.style.backgroundColor = 'rgba(233,216,194,0.7)';
    rootElement.style.width = '250px';
    rootElement.style.padding = '3px 5px';

    rootElement.appendChild(element);
  }

  return rootElement;
}
