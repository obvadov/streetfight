import { showModal } from './modal';

export function showWinnerModal(fighter) {
  showModal({
    title: 'We have a winner',
    bodyElement: fighter.name,
    onClose: () => {
      console.log("let's reload the page");
      location.reload();
    },
  });
}
