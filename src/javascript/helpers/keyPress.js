export default function specKeyPress(fn, keyUpFunc, ...keyCodes) {
  const keyPressed = new Set();
  document.addEventListener('keydown', function (event) {
    keyPressed.add(event.code);

    for (let code of keyCodes) {
      if (!keyPressed.has(code)) {
        return;
      }
    }

    fn();
  });

  document.addEventListener('keyup', function (event) {
    if (keyUpFunc && {}.toString.call(keyUpFunc) === '[object Function]') {
      keyUpFunc();
    }
    keyPressed.delete(event.code);
  });
}
